<?php
header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');

header('Content-Type: application/json');

require_once 'flight/Flight.php';
require_once 'auto_load.php';


use paragraph1\phpFCM\Client;
use paragraph1\phpFCM\Message;
use paragraph1\phpFCM\Recipient\Device;
use paragraph1\phpFCM\Notification;

//Firebird notification lib.
require_once 'vendor/autoload.php';

$apiKey = 'AAAAGRK2E-0:APA91bGgJtFxsU6aV6-eziFX4gApJyt2x1zg0nPyrLRI3chOxhDU0N3mygUgzQ3PrXK8D1lHw74c5VtfEyXvaM80V4_48KrACfSdjJhLRVaq4KFh7Es8IrSVRO-CzP8xOtD9SbMJkc-f';

$client = new Client();

use \Firebase\JWT\JWT;

    $payload =[
      'iss'=>'BBSOFT',
      'nbf'=>strtotime('2021-01-01 00:00:01'),
      'exp'=>strtotime('2021-01-01 00:00:01'),
  ];

  $token = JWT::encode($payload, '5f2b5cdbe5194f10b3241568fe4e2b24');


Database::engage(new MySQL());

$m = new Members();
$p = new Products();
$review = new Service_reviews();
$part = new Parts();
$post = new Posts();
$b = new Brands();
$porto = new Portfolio();
$module = new Modules();
$comment = new Comments();
$store = new Store();
$cat = new Category();
$ser = new Services();
$loc = new Locations();
$bskt = new Basket_req();
$offer = new offer();
$tasks = new Tasks_reviews();
$service_req = new service_req();
$notify = new notifications();

Flight::set('flight.views.path', './views/');

Flight::route('POST /notify/limit', function () use($notify){
    
    //Wait for Firebird request...    
    $data = Flight::request()->data;
    $notify->limit($data["m_id"],$data["limit"]);
});

/*
Flight::route('POST /notify/create', function()use($notify){
    
    //Wait for Firebird request...    
    $data = Flight::request()->data;
    				
    $notify->insert([
        $data['n_cat'],
        $data['n_desc'],
        $data['n_content'],
        $data['n_createdBy'],
        $data['n_cdate'],
        $data['is_seen']
    ]);
});
*/



Flight::route('POST /basket', function()use($bskt){
    
    //Wait for Firebird request...    
    $data = Flight::request()->data;
    $bskt->getItems($data["user_id"]);
});


Flight::route('POST /create/basket', function()use($bskt){
    
    //Wait for Firebird request...    
    $data = Flight::request()->data;
    				
    $bskt->insert([
        '',
        $data['bsk_items'],
        $data['bsk_date'],
        $data['m_id'],
        $data['is_handled']
    ]);
});


Flight::route('/members/login', function()use($m,$token){
    
    //prepare for JWT..
    
    $data = Flight::request()->data;

    $res = $m->login([$data['m_mobile'],$data['m_pwd']]);
    if($res){
    $res['token']=$token;
    print_r(json_encode($res));
    }else{
        print_r(json_encode(['message'=>'Wrong mobile No / password','status'=>0]));
    }
});

Flight::route('POST /member/last', function()use($m){
    var_dump($m->getLast());    
});


/* Locations */

Flight::route('POST /area/list', function()use($loc){
    $loc->getArea();    
});

Flight::route('POST /country/list', function()use($loc){
    $loc->getCountry();    
});

Flight::route('POST /area/list/country', function()use($loc){
	$data = Flight::request()->data;
    $loc->getAreaByCountry($data["country"]);    
});

Flight::route('/notify', function()use($m){

print_r($m->getFB_Token(38));

});

/* Members */
Flight::route('POST /members/new', function()use($m,$client,$apiKey){
    
    $data = Flight::request()->data;

    /*if($m->insertNew([
        '',
        $data['m_fname'],
        $data['m_lname'],
        $data['m_gender'],
        $data['m_status'],
        $data['m_account'],
        password_hash($data['m_pwd'],PASSWORD_BCRYPT),
        $data['m_mobile'],
        $data['m_email'],
        $data['m_country'],
        $data['m_area'],
        $data['m_address'],
        $data['m_utype'],
        $data['m_cdate'],
        $data['m_comment'],
        'img/unknow.jpg'
    ],$data['fb_token'])){

            //Firebase init.
        $client->setApiKey($apiKey);
        $client->injectHttpClient(new \GuzzleHttp\Client());

        $note = new Notification('test title', 'testing body');
        $note->setIcon('notification_icon_resource_name')
            ->setColor('#ffffff')
            ->setBadge(1);

        $message = new Message();
        $message->addRecipient(new Device($m->getFB_Token($m->getLast())));
        $message->setNotification($note)
            ->setData(array('someId' => 111));

        $response = $client->send($message);
        print_r($response->getStatusCode());


    }else{
        print_r(json_encode(['fireBird_token'=>$m->getFB_Token($m->getLast()),'Member_id'=>true]));
    }
    */

    $m->insertNew([
        '',
        $data['m_fname'],
        $data['m_lname'],
        $data['m_gender'],
        $data['m_status'],
        $data['m_account'],
        password_hash($data['m_pwd'],PASSWORD_BCRYPT),
        $data['m_mobile'],
        $data['m_email'],
        $data['m_country'],
        $data['m_area'],
        $data['m_address'],
        $data['m_utype'],
        $data['m_cdate'],
        $data['m_comment'],
        'img/unknow.jpg'
    ],$data['fb_token']);
    


});



Flight::route('POST /members/update_pwd', function()use($m){
//Flight::route('POST /haraj/eng/portfolio/@id', function($id)use($porto){
    //
    $data = Flight::request()->data;
    $m->update_pwd($data["id"],$data["new"],$data["old"]);
     //var_dump($data);
});


Flight::route('POST /members/update_profile', function()use($m){
    //Flight::route('POST /haraj/eng/portfolio/@id', function($id)use($porto){
        //
        $data = Flight::request()->data;
        $m->update_profile($data);
       // $m->update_profile($data["m_id"],$data["m_account"],$data["m_email"],$data["m_address"],$data["m_country"],$data["m_area"],
       // $data["m_img"]);
    });



Flight::route('PUT /members/update', function()use($m){
    
    $data = Flight::request()->data;

    $m->update([
        $data['m_fname'],
        $data['m_lname'],
        $data['m_gender'],
        $data['m_status'],
        $data['m_account'],
        password_hash($data['m_pwd'],PASSWORD_BCRYPT),
        $data['m_mobile'],
        $data['m_email'],
        $data['m_country'],
        $data['m_area'],
        $data['m_address'],
        $data['m_utype'],
        $data['m_cdate'],
        $data['m_comment'],
        'img/unknow.jpg'
    ],$data['m_id']);

});


Flight::route('POST /products/create', function()use($p){
    
    $data = Flight::request()->data;

 $img_list='';
 if(!is_string($data['p_img'])){

   $countfiles = count($_FILES['p_img']['name']);

   if($countfiles>0){
    for($i=0;$i<$countfiles;$i++){
      $new_img_name= 'img_'.rand(1,999999);
      $file_name=explode('.',$_FILES['p_img']['name'][$i]);
      $ext=end($file_name);
      $img_list.=$config['env']['PROJECT_PATH'].$config['env']['UPLOAD_DIR'].rtrim($new_img_name.'.'.$ext,',').',';
      move_uploaded_file($_FILES['p_img']['tmp_name'][$i],$config['env']['UPLOAD_DIR'].$new_img_name.'.'.$ext);
  
     }
    }else{
        $img_list='';
    }
 }
 

    $p->insert([
        '',
        $data['p_name'],
        $data['is_sold'],
        $data['p_model'],
        $data['vin_no'],
        $data['is_new'],
        $data['p_mfr'],
        $data['p_detailes'],
        $data['p_comment'],
        $data['p_cdate'],
        $data['p_brand'],
        $data['p_currency'],
        $data['p_price'],
        //$data['p_img'],
        $img_list,
        $data['is_available'],
        $data['is_haraj'],
        $data['p_createdBy'],
        $data['p_tag'],
        $data['p_mileage'],
        $data['p_trans'],
        $data['p_fuel'],
        $data['p_year'],
        $data['is_orginal'],
        $data['p_store']
    ]);

});

Flight::route('POST /products/list', function()use($p){
    Flight::etag('limit');
	 $data = Flight::request()->data;
    $p->limit($data["limit"]);    
});

/*Flight::route('GET /products/haraj/@limit', function($limit)use($p){
    $p->isHaraj($limit);    
});*/

Flight::route('POST /products/store', function()use($p){
    Flight::etag('limit');
	 $data = Flight::request()->data;
    $p->isStore($data["limit"]);    
});

Flight::route('POST /products/brand/isnew', function()use($p){
    Flight::etag('limit');
    Flight::etag('isNew');
	$data = Flight::request()->data;
    $p->theNew($data["brand"],$data["isNew"],$data["limit"]);    
});

Flight::route('POST /products/brand', function()use($p){
    Flight::etag('limit');
    Flight::etag('brand');
	$data = Flight::request()->data;
    $p->getByBrand($data["brand"],$data["limit"]);    
});


Flight::route('POST /brands/list', function()use($b){
    $b->getBrands();    
});


Flight::route('POST /module', function()use($module){
	$data = Flight::request()->data;
    $module->getModules($data["brandID"]);    
});


/* Cars Parts*/


Flight::route('POST /parts/create', function()use($part){
    
    $data = Flight::request()->data;


 $img_list='';
 if(!is_string($data['pr_img'])){

   $countfiles = count($_FILES['pr_img']['name']);

   if($countfiles>0){
    for($i=0;$i<$countfiles;$i++){
      $new_img_name= 'img_'.rand(1,999999);
      $file_name=explode('.',$_FILES['pr_img']['name'][$i]);
      $ext=end($file_name);
      $img_list.=$config['env']['PROJECT_PATH'].$config['env']['UPLOAD_DIR'].rtrim($new_img_name.'.'.$ext,',').',';
      move_uploaded_file($_FILES['pr_img']['tmp_name'][$i],$config['env']['UPLOAD_DIR'].$new_img_name.'.'.$ext);
  
     }
    }else{
        $img_list='';
    }
 }
 

    $part->insert([
        '',
        $data['pr_name'],
        $data['is_sold'],
        $data['pr_model'],
        $data['vin_no'],
        $data['is_new'],
        $data['pr_mfr'],
        $data['pr_detailes'],
        $data['pr_comment'],
        $data['pr_cdate'],
        $data['pr_brand'],
        $data['pr_currency'],
        $data['pr_price'],
        //$data['pr_img'],
        $img_list,
        $data['is_available'],
        $data['is_haraj'],
        $data['pr_createdBy'],
        $data['pr_tag'],
        $data['pr_year'],
        $data['pr_type'],
        $data['is_orginal'],
        $data['pr_store']
    ]);

});



Flight::route('POST /parts/list', function()use($part){
    Flight::etag('limit');
	$data = Flight::request()->data;
    $part->limit($data["limit"]);    
});


Flight::route('POST /parts/haraj', function()use($part){
    Flight::etag('limit');
	$data = Flight::request()->data;
    $part->isHaraj($data["limit"]);    
});

Flight::route('POST /parts/store', function()use($part){
    Flight::etag('limit');
	$data = Flight::request()->data;
    $part->isStore($data["limit"]);    
});


Flight::route('POST /parts/types', function()use($part){
    $part->partType();    
});


/* Comments */

Flight::route('POST /comment/create', function()use($comment){
    
    $data = Flight::request()->data;

    $comment->insert([
        '',
        $data['cmnt_content'],
        $data['cmnt_by'],
        $data['cmnt_post_id'],
        $data['cmnt_date'],
    ]);

});

Flight::route('POST /comments', function()use($comment){
	$data = Flight::request()->data;
    $comment->commentByPostID($data["post_id"],$data["limit"]);    
});


/* Posts */

Flight::route('POST /posts/create', function()use($post,$config){
    
    $data = Flight::request()->data;
    //$request = Flight::request();
    //$file = $request->files['img'];

    //print_r($request->data);
    
 $img_list='';
 if(!is_string($data['post_img'])){

   $countfiles = count($_FILES['post_img']['name']);

   if($countfiles>0){
    for($i=0;$i<$countfiles;$i++){
      $new_img_name= 'img_'.rand(1,999999);
      $file_name=explode('.',$_FILES['post_img']['name'][$i]);
      $ext=end($file_name);
      $img_list.=$config['env']['PROJECT_PATH'].$config['env']['UPLOAD_DIR'].rtrim($new_img_name.'.'.$ext,',').',';
      move_uploaded_file($_FILES['post_img']['tmp_name'][$i],'upload/'.$new_img_name.'.'.$ext);
  
     }
    }else{
        $img_list='';
    }
 }
   
    //var_dump(rtrim($img_list,','));
  
    $post->insert([
        '',
        $data['post_title'],
        $data['post_thumb'],
        $data['post_desc'],
        $data['post_content'],
        $img_list,
        $data['post_cat'],
        $data['post_price'],
        $data['post_cur'],
        date('Y-m-d H:i:s'),
        $data['post_status'],
        $data['post_view_count'],
        $data['post_createdBy'],
        $data['p_type'],
        $data['p_period']
    ]);

});


Flight::route('POST /haraj/post', function()use($post){
    Flight::etag('limit');
	$data = Flight::request()->data;
    $post->postList($data["limit"]);
});

Flight::route('POST /haraj/post/type', function()use($post){
    Flight::etag('limit');
    Flight::etag('type');
	$data = Flight::request()->data;
    $post->postListByType($data["type"],$data["limit"]);
});

Flight::route('POST /haraj/post/member', function()use($post){
    Flight::etag('limit');
    Flight::etag('member_id');
    Flight::etag('type');
	$data = Flight::request()->data;
    $post->postListByMember($data["member_id"],$data["type"],$data["limit"]);
});


/* Reviews [Srvices] */
Flight::route('POST /service/reviews/create', function()use($review){
    $data = Flight::request()->data;
    $review->insert([
        $data['s_id'],
        $data['m_id'],
        $data['review'],
        $data['stars'],
        date('Y-m-d H:i:s')
    ]);
});


Flight::route('POST /service/reviews', function()use($review){
    Flight::etag('limit');
    Flight::etag('service_id');
	$data = Flight::request()->data;
    $review->getServiceReviews($data["server_id"],$data["limit"]);
});

/* ----------------------------------------------------------------------------------- */


/* Reviews [Tasks] */
Flight::route('POST /task/reviews/create', function()use($tasks){
    $data = Flight::request()->data;
    $tasks->insert([
        $data['s_id'],
        $data['m_id'],
        $data['review'],
        $data['stars'],
        date('Y-m-d H:i:s')
    ]);
});


Flight::route('POST /task/reviews', function()use($tasks){
    Flight::etag('limit');
    Flight::etag('task_id');
	$data = Flight::request()->data;
    $tasks->getTaskReviews($data["task_id"],$data["limit"]);
});



/* Store */

Flight::route('POST /store/create', function()use($store){
    
    $data = Flight::request()->data;

    $store->insert([
        '',
        $data['store_title'],
        $data['store_address'],
        $data['store_latlang'],
        $data['store_owner'],
        $data['store_cdate'],
        $data['store_area']
    ]);

});



Flight::route('POST /store/list', function()use($store){
    Flight::etag('limit');
    Flight::etag('area');
	$data = Flight::request()->data;
    $store->view($data["limit"],$data["area"]);
});


Flight::route('POST /store/deleviry', function()use($store){
    $data = Flight::request()->data;
     //print_r((string)$data['stores_id']);
    foreach ((object)$data['stores_id'] as $key => $value) {
        echo $value.'£';
    }
    //$store->deleviry(json_decode($data['stores_id']));
});

/* eng */

Flight::route('POST /eng/specs', function()use($porto){
    $porto->spec();
});

Flight::route('POST /eng/spec', function()use($m){
    Flight::etag('limit');
    Flight::etag('spec');
	$data = Flight::request()->data;
    $m->engBySpec($data["spec"],$data["limit"]);
});

Flight::route('POST /haraj/eng', function()use($m){
    Flight::etag('limit');
	$data = Flight::request()->data;
    $m->eng($data["limit"]);
});


Flight::route('POST /haraj/eng/member', function()use($m){
    Flight::etag('limit');
    Flight::etag('member_id');
	$data = Flight::request()->data;
    $m->engById($data["member_id"],$data["limit"]);
});



Flight::route('POST /haraj/eng/portfolio/@id', function($id)use($porto){
    
    $data = Flight::request()->data;

    $porto->modify([
        $id,
        $data['u_skills'],
        $data['u_cert'],
        $data['u_defination'],
        $data['u_accomplishment'],
        $data['u_spec']
    ],$id);

});


/*Category*/

Flight::route('POST /haraj/cat', function()use($cat){
	$data = Flight::request()->data;
    $cat->cat($data["id"]);
});


/* Product type */

Flight::route('POST /haraj/product-type', function()use($cat){
    $cat->productType();
});


/*services*/

Flight::route('POST /service/create', function()use($ser){
    
    $data = Flight::request()->data;

    $ser->insert([
        '',
        $data['ser_type'],
        $data['ser_periods'],
        $data['ser_address'],
        $data['ser_latlang'],
        $data['ser_owner'],
        $data['ser_details'],
        $data['ser_area'],
        date('Y-m-d H:i:s'),
        $data['ser_country'],
        $data['ser_thumb']
    ]);
});

Flight::route('POST /service/request/create', function()use($service_req){
    $data = Flight::request()->data;
    $service_req->insert([
        '',
        $data['m_id'],
        date('Y-m-d H:i:s'),
        $data['aprove'],
        $data['note']
    ]);
});

Flight::route('POST /service/requests', function()use($service_req){
    Flight::etag('service_id');
	$data = Flight::request()->data;
    $service_req->getAllRequests($data["service_id"]);
});



Flight::route('POST /service/area', function()use($ser){
    Flight::etag('limit');
    Flight::etag('area');
	$data = Flight::request()->data;
    $ser->viewByArea($data["area"],$data["limit"]);
});

Flight::route('POST /service/country', function()use($ser){
    Flight::etag('limit');
    Flight::etag('country');
	$data = Flight::request()->data;
    $ser->viewByCountry($data["country"],$data["limit"]);
});

Flight::route('POST /service/type', function()use($ser){
    Flight::etag('limit');
    Flight::etag('type');
    Flight::etag('country');
	$data = Flight::request()->data;
    $ser->viewByType($data["type"],$data["country"],$data["limit"]);
});


Flight::route('POST /service/types', function()use($ser){
   $ser->viewAllType();
});


/* Calculate stars by grouping by service id*/

Flight::route('POST /service/area/list', function()use($review){
    Flight::etag('limit');
    Flight::etag('area');
	$data = Flight::request()->data;
    $review->listServicesByArea($data["area"],$data["limit"]);
});

Flight::route('POST /service/country/list', function()use($review){
	$data = Flight::request()->data;
    $review->listServicesByCountry($data["country"],$data["limit"]);
});

/*Offers*/

Flight::route('POST /create/offer/private', function()use($offer,$m,$notify,$client,$apiKey){
    
    $data = Flight::request()->data;
	
	$client->setApiKey($apiKey);
     $client->injectHttpClient(new \GuzzleHttp\Client());
  
     $note = new Notification('عرض عمل', $m->getMemberNameByID($data['o_owner']).' لدية عرض عمل لك ');
     $note->setIcon('notification_icon_resource_name')
         ->setColor('#ffffff')
         ->setBadge(1);
 
     $message = new Message();
     $message->addRecipient(new Device($m->getFB_Token($data['o_emp'])));
     $message->setNotification($note)
         ->setData(array('someId' => 111));
 
     $response = $client->send($message);
	 
    $offer->privateOffer($data['post_createdBy'],[
        '',
        $data['post_title'],
        $data['post_thumb'],
        $data['post_desc'],
        $data['post_content'],
        "",
        $data['post_cat'],
        $data['post_price'],
        $data['post_cur'],
        date('Y-m-d H:i:s'),
        $data['post_status'],
        $data['post_view_count'],
        $data['post_createdBy'],
        $data['p_type'],
        //4,
        $data['p_period']
    ]
    ,
    ['',
    $data['post_createdBy'],
    $data['o_emp'],
    $data['o_sdate'],
    $data['o_edate'],
    $data['o_note'],
    $data['o_progress']
    ]
   );

});


Flight::route('POST /create/offer', function()use($m,$offer,$notify,$client,$apiKey){
    
    $data = Flight::request()->data;
                    
    
     //$m->getFB_Token($data['m_id']);
     //Firebase init.
     $client->setApiKey($apiKey);
     $client->injectHttpClient(new \GuzzleHttp\Client());
  
     $note = new Notification('عرض عمل', $m->getMemberNameByID($data['o_owner']).' لدية عرض عمل لك ');
     $note->setIcon('notification_icon_resource_name')
         ->setColor('#ffffff')
         ->setBadge(1);
 
     $message = new Message();
     $message->addRecipient(new Device($m->getFB_Token($data['o_emp'])));
     $message->setNotification($note)
         ->setData(array('someId' => 111));
 
     $response = $client->send($message);


    $offer->insert([
        '',
        $data['o_owner'],
        $data['o_emp'],
        $data['o_sdate'],
        $data['o_edate'],
        $data['o_note'],
        $data['o_progress'],
        $data['p_id']
    ]);

    $offer->update_post($data['post_id'], 4);

    if($notify->insertNotification([
        $data['n_cat'],
        $data['n_desc'],
        $data['n_content'],
        $data['o_owner'],
        $data['n_cdate'],
        $data['is_seen'],
        $data['o_emp']
    ])
    ){
      
        $response = $client->send($message);

    }else{
        echo 'Something goes wrong';
    }

});


Flight::route('POST /offer/progress/eng', function()use($offer){
	$data = Flight::request()->data;
    $offer->getOfferProgress($data["member_id"]);
});

Flight::route('POST /offer/progress/owner', function()use($offer){
	$data = Flight::request()->data;
    $offer->getOfferProgressByOwner($data["member_id"]);
});


Flight::route('PUT /offer/update/@offer_id/@status', function($offer_id,$status)use($offer){
    
    $data = Flight::request()->data;
    					
    $offer->updateProgress($data['o_id'],$data['o_progress']);

});


Flight::start();
