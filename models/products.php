<?php

class Products extends Database{
    
    public function __construct(){}


    public static function limit($limit){
        //var_dump(self::$dbObject);
        print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select p_id, p_name,brand.b_title as brand ,is_orginal,is_sold, vin_no, is_new, p_mfr, p_detailes, p_comment, p_cdate, currency.cu_comment as currency, p_price, p_img, is_available, is_haraj, members.m_fname fname, members.m_lname as lname,members.m_img as avatar, p_tag, p_mileage,models.mo_title as model, trans.tr_trype as trans, fuel_type.f_type as fuel, p_year,store_id,store_title,store_address,store_latlang,store_detial,store_area,store_owner,store_status,users.u_account,area.area_name from products INNER JOIN members ON products.p_createdBy = members.m_id INNER JOIN currency ON products.p_currency = currency.cu_id INNER JOIN fuel_type ON products.p_fuel = fuel_type.f_id INNER JOIN trans ON products.p_trans = trans.tr_id JOIN models on products.p_model=models.mo_id JOIN brand on products.p_brand = brand.b_id INNER JOIN store ON products.p_store = store.store_id INNER JOIN area on area.area_id=store.store_area INNER JOIN users on users.u_id=store.store_owner ORDER BY p_id DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
      }
     
      public static function theNew($brand,$isNew,$limit){
        //var_dump(self::$dbObject);
        print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select p_id, p_name,brand.b_title as brand ,is_orginal,is_sold, vin_no, is_new, p_mfr, p_detailes, p_comment, p_cdate, currency.cu_comment as currency, p_price, p_img, is_available, is_haraj, members.m_fname fname, members.m_lname as lname,members.m_img as avatar, p_tag, p_mileage,models.mo_title as model, trans.tr_trype as trans, fuel_type.f_type as fuel, p_year,store_id,store_title,store_address,store_latlang,store_detial,store_area,store_owner,store_status,users.u_account,area.area_name from products INNER JOIN members ON products.p_createdBy = members.m_id INNER JOIN currency ON products.p_currency = currency.cu_id INNER JOIN fuel_type ON products.p_fuel = fuel_type.f_id INNER JOIN trans ON products.p_trans = trans.tr_id JOIN models on products.p_model=models.mo_id JOIN brand on products.p_brand = brand.b_id INNER JOIN store ON products.p_store = store.store_id INNER JOIN area on area.area_id=store.store_area INNER JOIN users on users.u_id=store.store_owner where products.p_brand=".$brand." and products.is_new=".$isNew." ORDER BY p_id DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
      }

      public static function getByBrand($brand,$limit){
        //var_dump(self::$dbObject);
        print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select p_id, p_name,brand.b_title as brand ,is_orginal,is_sold, vin_no, is_new, p_mfr, p_detailes, p_comment, p_cdate, currency.cu_comment as currency, p_price, p_img, is_available, is_haraj, members.m_fname fname, members.m_lname as lname,members.m_img as avatar, p_tag, p_mileage,models.mo_title as model, trans.tr_trype as trans, fuel_type.f_type as fuel, p_year,store_id,store_title,store_address,store_latlang,store_detial,store_area,store_owner,store_status,users.u_account,area.area_name from products INNER JOIN members ON products.p_createdBy = members.m_id INNER JOIN currency ON products.p_currency = currency.cu_id INNER JOIN fuel_type ON products.p_fuel = fuel_type.f_id INNER JOIN trans ON products.p_trans = trans.tr_id JOIN models on products.p_model=models.mo_id JOIN brand on products.p_brand = brand.b_id INNER JOIN store ON products.p_store = store.store_id INNER JOIN area on area.area_id=store.store_area INNER JOIN users on users.u_id=store.store_owner where products.p_brand=".$brand." ORDER BY p_id DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
      }

      public static function isHaraj($limit){
        print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select p_id, p_name,brand.b_title as brand ,is_orginal,is_sold, vin_no, is_new, p_mfr, p_detailes, p_comment, p_cdate, currency.cu_comment as currency, p_price, p_img, is_available, is_haraj, members.m_fname fname, members.m_lname as lname,members.m_img as avatar, p_tag, p_mileage,models.mo_title as model, trans.tr_trype as trans, fuel_type.f_type as fuel, p_year,store_id,store_title,store_address,store_latlang,store_detial,store_area,store_owner,store_status,users.u_account,area.area_name from products INNER JOIN members ON products.p_createdBy = members.m_id INNER JOIN currency ON products.p_currency = currency.cu_id INNER JOIN fuel_type ON products.p_fuel = fuel_type.f_id INNER JOIN trans ON products.p_trans = trans.tr_id JOIN models on products.p_model=models.mo_id JOIN brand on products.p_brand = brand.b_id INNER JOIN store ON products.p_store = store.store_id INNER JOIN area on area.area_id=store.store_area INNER JOIN users on users.u_id=store.store_owner where is_haraj= 1 ORDER BY p_id DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null);
      }

      public static function isStore($limit){
        print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select p_id, p_name,brand.b_title as brand ,is_orginal,is_sold, vin_no, is_new, p_mfr, p_detailes, p_comment, p_cdate, currency.cu_comment as currency, p_price, p_img, is_available, is_haraj, members.m_fname fname, members.m_lname as lname,members.m_img as avatar, p_tag, p_mileage,models.mo_title as model, trans.tr_trype as trans, fuel_type.f_type as fuel, p_year,store_id,store_title,store_address,store_latlang,store_detial from products INNER JOIN members ON products.p_createdBy = members.m_id INNER JOIN currency ON products.p_currency = currency.cu_id INNER JOIN fuel_type ON products.p_fuel = fuel_type.f_id INNER JOIN trans ON products.p_trans = trans.tr_id JOIN models on products.p_model=models.mo_id JOIN brand on products.p_brand = brand.b_id INNER JOIN store ON products.p_store = store.store_id where is_haraj= 0 ORDER BY p_id DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null);
      }

}

?>