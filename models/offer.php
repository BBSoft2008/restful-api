<?php

class Offer extends Database{
    
    public function __construct(){}

      public function getOfferProgress($member){
        print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select *,og_status,post_title,post_desc,post_content,post_img from offer INNER JOIN offer_progress on offer.o_progress =offer_progress.og_id INNER JOIN posts on offer.p_id =posts.post_id where o_emp=".$member." and (posts.p_type=3 OR posts.p_type=4)")->fetchall(PDO::FETCH_ASSOC)):null); 
      }


      public function getOfferProgressByOwner($member){
        print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select *,og_status,post_title,post_desc,post_content,post_img from offer INNER JOIN offer_progress on offer.o_progress =offer_progress.og_id INNER JOIN posts on offer.p_id =posts.post_id where o_owner=".$member." and (posts.p_type=3 OR posts.p_type=4)")->fetchall(PDO::FETCH_ASSOC)):null); 
      }


      public function updateProgress($o_id,$o_status){
        $sql = "Update offer set o_progress = ".$o_status. " Where o_id =" . $o_id;
              $res = self::$dbObject->query($sql);
              if($res->rowCount()>0)
                 print_r(json_encode(['message'=>'Modified successfully.','status'=>true]));
              else
                 print_r(json_encode(['message'=>$res->errorCode(),'status'=>false]));
      }

      public function update_post($post_id,$post_status){
        $sql = "Update posts set post_status = ".$post_status. " Where post_id =" . $post_id;
              $res = self::$dbObject->query($sql);
              if($res->rowCount()>0)
                 print_r(json_encode(['message'=>'Modified successfully.','status'=>true]));
              else
                 print_r(json_encode(['message'=>$res->errorCode(),'status'=>false]));
      }

      public static function createPrivatePost($query = []){
        //get used class name as table name in query.
        //$table = end(explode('\\',get_called_class()));
        $sql='insert into posts()values(';
      
        foreach($query as $val){
             $sql.='?,';
        }
      
        $sql=rtrim($sql,',');
        $sql.=')';
      
       // echo $sql;
        $res = self::$dbObject->prepare($sql);
        if($res->execute($query))
         return 1;
        else
           return 0;
      
      }


      public function privateOffer($member_id, $privatePost=[],$offer=[]){
         if(self::createPrivatePost($privatePost)){
          $post_id = self::$dbObject->query('select post_id from posts where post_createdBy ='.$member_id.' and p_type= 4 order by post_id desc')->fetch(PDO::FETCH_ASSOC)['post_id'];  
              
          $sql='insert into offer()values(';
      
          foreach($offer as $val){
               $sql.='?,';
          }
        
          $sql.=$post_id.')';
         
         // echo $sql;
          $res = self::$dbObject->prepare($sql);
          if($res->execute($offer))
            print_r(json_encode(['message'=>'Inserted successfully.','status'=>true]));
          else
             print_r(json_encode(['message'=>$res->errorCode(),'status'=>false]));


         }else{
         echo print_r(json_encode(['message'=>'Wrong','status'=>false]));
         }

      }
}

?>