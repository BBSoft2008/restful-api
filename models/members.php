<?php

class Members extends Database{
    
    public function __construct(){}

    public function login($data=[]){
        $res = self::$dbObject->query('select members.m_id as m_id, m_fname, m_lname, m_gender, m_country, m_area, m_status, m_account, m_pwd, m_mobile, m_email, country.c_name, area.area_name, m_address, m_utype, m_cdate, m_comment, m_img, fb_tokens.fb_token as fb_token FROM members INNER join country ON members.m_country = country.c_id INNER JOIN area ON members.m_area = area.area_id INNER JOIN fb_tokens ON members.m_id = fb_tokens.m_id WHERE members.m_mobile='.$data[0])->fetch(PDO::FETCH_ASSOC);
        if(password_verify($data[1],$res['m_pwd'])){
           unset($res['m_pwd']);
           return $res;
        }else
           return false;
      }
    
      public static function getFB_Token($id){
         return self::$dbObject->query("select fb_token from fb_tokens where m_id=".$id)->fetchall(PDO::FETCH_ASSOC)['fb_token']; 
      }

      public static function getMemberNameByID($id){
         return self::$dbObject->query("select m_account from members where m_id=".$id)->fetchall(PDO::FETCH_ASSOC)['m_account']; 
      }

      public static function eng($limit){
         //var_dump(self::$dbObject);
         print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select m_id, m_fname, m_lname, m_gender, m_country, m_area, m_status, m_account, m_pwd, m_mobile, m_email, country.c_name, area.area_name, m_address, m_utype, m_cdate, m_comment, m_img,portfolio.u_skills as skills,portfolio.u_cert as cert,portfolio.u_defination as def,portfolio.u_accomplishment as acc,portfolio.u_spec as spec,specialization.spec_title as specTitle FROM members INNER join country ON members.m_country = country.c_id INNER JOIN area ON members.m_area = area.area_id INNER JOIN portfolio ON members.m_id = portfolio.u_id INNER JOIN specialization ON portfolio.u_spec = specialization.spec_id where m_utype= 4 ORDER BY m_cdate DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
       }

       public static function engById($id,$limit){
         //var_dump(self::$dbObject);
         print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select m_id, m_fname, m_lname, m_gender, m_country, m_area, m_status, m_account, m_pwd, m_mobile, m_email, country.c_name, area.area_name, m_address, m_utype, m_cdate, m_comment, m_img,portfolio.u_skills as skills,portfolio.u_cert as cert,portfolio.u_defination as def,portfolio.u_accomplishment as acc,portfolio.u_spec as spec,specialization.spec_title as specTitle FROM members INNER join country ON members.m_country = country.c_id INNER JOIN area ON members.m_area = area.area_id INNER JOIN portfolio ON members.m_id = portfolio.u_id INNER JOIN specialization ON portfolio.u_spec = specialization.spec_id where m_utype= 4 and m_id=".$id." ORDER BY m_cdate DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
       }

       public static function engBySpec($spec_id,$limit){
         //var_dump(self::$dbObject);
         print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select m_id, m_fname, m_lname, m_gender, m_country, m_area, m_status, m_account, m_pwd, m_mobile, m_email, country.c_name, area.area_name, m_address, m_utype, m_cdate, m_comment, m_img,portfolio.u_skills as skills,portfolio.u_cert as cert,portfolio.u_defination as def,portfolio.u_accomplishment as acc,portfolio.u_spec as spec,specialization.spec_title as specTitle FROM members INNER join country ON members.m_country = country.c_id INNER JOIN area ON members.m_area = area.area_id INNER JOIN portfolio ON members.m_id = portfolio.u_id INNER JOIN specialization ON portfolio.u_spec = specialization.spec_id where m_utype= 4 and portfolio.u_spec=".$spec_id." ORDER BY m_cdate DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
       }

       public static function update_pwd($id,$new_pwd,$old_pwd){
          $sql = " Select m_pwd  from  members where m_id = " . $id; // members // m_id
          $res = self::$dbObject->query($sql);
          if($res->rowCount()>0)
          {
      
            //$pass = $res->fetch(PDO::FETCH_ASSOC);
           
            if(password_verify($old_pwd,$res->fetch(PDO::FETCH_ASSOC)['m_pwd'])){
               $sql = "Update members set m_pwd = '".  password_hash($new_pwd ,PASSWORD_BCRYPT). "' Where m_id =" . $id;
               $res = self::$dbObject->query($sql);
               if($res->rowCount()>0)
                  print_r(json_encode(['message'=>'Modified successfully.' .$sql,'status'=>true]));
               else
                  print_r(json_encode(['message'=>$res->errorCode(),'status'=>false]));
            }
            else 
             print_r(json_encode(['message'=>"old pwd not true",'status'=>false]));
              }
            else
               print_r(json_encode(['message'=>$res->errorCode().$sql,'status'=>false]));
       }

       //public static function update_profile($m_id,$m_account,$m_email,$m_address,$m_country,$m_area,$m_img){
         
       public static function update_profile($data = []){
         $sql = "Select *  from  members where m_id = " . $data["m_id"]; // members // m_id
         $res = self::$dbObject->query($sql);
         if($res->rowCount()>0)
         {
              $sql = "Update members set m_account = '".  $data["m_account"]. "',m_email = '".  $data["m_email"]. "' , m_address = '".  $data["m_address"]
              . "',m_country = ".  $data["m_country"]." , m_area = ".  $data["m_area"]. ",m_img = '".  $data["m_img"]. "' Where m_id =" . $data["m_id"];
              $res = self::$dbObject->query($sql);
              if($res->rowCount()>0)
                 print_r(json_encode(['message'=>'Modified successfully.','status'=>true]));
              else
                 print_r(json_encode(['message'=>$res->errorCode(),'status'=>false]));
            }
           else
              print_r(json_encode(['message'=>$res->errorCode(),'status'=>false]));
      }

    public static function getLast(){
       return self::$dbObject->query("Select m_id from members order by m_id desc")->fetch(PDO::FETCH_ASSOC)['m_id'];
    }




    public static function insertNew($query = [],$fb_token){
      //get used class name as table name in query.
      //$table = end(explode('\\',get_called_class()));
      $sql='insert into '.strtolower(get_called_class()).'()values(';
    
      foreach($query as $val){
           $sql.='?,';
      }
    
      $sql=rtrim($sql,',');
      $sql.=')';
    
     // echo 'insert into fb_tokens()values("","'.$fb_token.'",'.self::getLast().')';
     // echo $sql;
      $res = self::$dbObject->prepare($sql);
      if($res->execute($query)){
          $t = self::$dbObject->prepare('insert into fb_tokens()values(?,?,?)');
         if($t->execute(['',$fb_token,self::getLast()])){
               print_r(json_encode(['message'=>'Created successfully.','status'=>true]));
            }else{
                print_r(json_encode(['message'=>$res->errorCode(),'status'=>false]));
            }
      }else{

      }
    }

    

}

?>