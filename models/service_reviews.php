<?php

class Service_reviews extends Database{
    
    public function __construct(){}


    public static function getServiceReviews($server_id,$limit){
      //var_dump(self::$dbObject);
      print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select review,stars,rv_date,m_account,m_img from service_reviews INNER JOIN members ON service_reviews.m_id = members.m_id where s_id=".$server_id." ORDER BY s_id DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
    }


    public static function listServicesByCountry($country_id,$limit){
      print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select review,(sum(stars)/count(stars))as star,rv_date,m_account,m_img,services_type.sert_type as stype from service_reviews INNER JOIN members ON service_reviews.m_id = members.m_id INNER JOIN services ON service_reviews.s_id=services.ser_id INNER JOIN services_type ON services.ser_type = services_type.sert_id WHERE services.ser_country =".$country_id." GROUP BY s_id limit ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
    } 

    public static function listServicesByArea($area_id,$limit){
      print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select review,(sum(stars)/count(stars))as star,rv_date,m_account,m_img,services_type.sert_type as stype from service_reviews INNER JOIN members ON service_reviews.m_id = members.m_id INNER JOIN services ON service_reviews.s_id=services.ser_id INNER JOIN services_type ON services.ser_type = services_type.sert_id WHERE services.ser_country =".$area_id." GROUP BY s_id limit ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
    }
}

?>