<?php

class Services extends Database{
    
    public function __construct(){}

    /*Updated Method Functionality*/
    public static function viewByArea($area_id,$limit){
      //var_dump(self::$dbObject);
      print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select review,(sum(stars)/count(stars))as star,rv_date,m_account,m_img,services_type.sert_type as stype from service_reviews INNER JOIN members ON service_reviews.m_id = members.m_id INNER JOIN services ON service_reviews.s_id=services.ser_id INNER JOIN services_type ON services.ser_type = services_type.sert_id WHERE services.ser_country =".$area_id." GROUP BY s_id LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
    }
     
    public static function viewByCountry($country_id,$limit){
      //var_dump(self::$dbObject);
      print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select review,(sum(stars)/count(stars))as star,rv_date,m_account,m_img,services_type.sert_type as stype from service_reviews INNER JOIN members ON service_reviews.m_id = members.m_id INNER JOIN services ON service_reviews.s_id=services.ser_id INNER JOIN services_type ON services.ser_type = services_type.sert_id WHERE services.ser_country =".$country_id." GROUP BY s_id LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
    }

    public static function viewByType($type,$country_id,$limit){
      //var_dump(self::$dbObject);
      print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select *,area.area_name as area,area.area_id as area_id,country.c_name as countryName from services INNER JOIN services_type ON services.ser_type = services_type.sert_id INNER JOIN area ON services.ser_area= area.area_id INNER JOIN country ON services.ser_country= country.c_id where ser_country=".$country_id." and ser_type=".$type." ORDER BY ser_id DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
    }
    
    public static function viewAllType(){
      //var_dump(self::$dbObject);
      print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select * from services_type")->fetchall(PDO::FETCH_ASSOC)):null); 
    }


}

?>