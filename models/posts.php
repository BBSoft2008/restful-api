<?php

class Posts extends Database{
    
    public function __construct(){}

    public static function postList($limit){
        //var_dump(self::$dbObject);
        $sql = "select post_createdBy,post_id,post_title,post_thumb,post_desc,post_cat,area.area_name as area,post_content,post_img,
        category.ca_type,post_price,currency.cu_currency as currency,post_cdate,post_status,post_view_count,members.m_fname as 
        firstName,members.m_lname as LastName,members.m_account as full_name,posts.p_type, category.p_type as ct_type from posts
         INNER JOIN category ON posts.post_cat = category.ca_id INNER JOIN currency ON posts.post_cur = currency.cu_id INNER JOIN
          members ON posts.post_createdBy =members.m_id INNER JOIN area ON members.m_area= area.area_id WHERE posts.p_type =1 OR posts.p_type=2 ORDER BY post_cdate DESC LIMIT ".$limit;
      //  print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select post_id,post_title,post_thumb,post_desc,post_cat,area.area_name as area,post_content,post_img,category.ca_type,post_price,currency.cu_currency as currency,post_cdate,post_status,post_view_count,members.m_fname as firstName,members.m_lname as LastName,members.m_account as full_name,p_type from posts INNER JOIN category ON posts.post_cat = category.ca_id INNER JOIN currency ON posts.post_cur = currency.cu_id INNER JOIN members ON posts.post_createdBy=members.m_id INNER JOIN area ON members.m_area= area.area_id ORDER BY post_cdate DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
        print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query($sql)->fetchall(PDO::FETCH_ASSOC)):null); 
      }


      public static function postListByType($type,$limit){
        //var_dump(self::$dbObject);
        $sql = "select post_createdBy,post_id,post_title,post_thumb,post_desc,post_cat,area.area_name as area,post_content,post_img,
        category.ca_type,post_price,currency.cu_currency as currency,post_cdate,post_status,post_view_count,members.m_fname as 
        firstName,members.m_lname as LastName,members.m_account as full_name,posts.p_type as ptype, category.p_type as ct_type from posts
         INNER JOIN category ON posts.post_cat = category.ca_id INNER JOIN currency ON posts.post_cur = currency.cu_id INNER JOIN
          members ON posts.post_createdBy =members.m_id INNER JOIN area ON members.m_area= area.area_id where posts.p_type=".$type."
          AND post_status = 2 ORDER BY post_cdate DESC LIMIT ".$limit;
      //  print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select post_id,post_title,post_thumb,post_desc,post_cat,area.area_name as area,post_content,post_img,category.ca_type,post_price,currency.cu_currency as currency,post_cdate,post_status,post_view_count,members.m_fname as firstName,members.m_lname as LastName,members.m_account as full_name,p_type from posts INNER JOIN category ON posts.post_cat = category.ca_id INNER JOIN currency ON posts.post_cur = currency.cu_id INNER JOIN members ON posts.post_createdBy=members.m_id INNER JOIN area ON members.m_area= area.area_id ORDER BY post_cdate DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
        print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query($sql)->fetchall(PDO::FETCH_ASSOC)):null); 
      }


      public static function postListByMember($member_id,$type,$limit){
        //var_dump(self::$dbObject);
        $sql = "select post_createdBy,post_id,post_title,post_thumb,post_desc,post_cat,area.area_name as area,post_content,post_img,post_createdBy,
        category.ca_type,post_price,currency.cu_currency as currency,post_cdate,post_status,post_view_count,members.m_fname as 
        firstName,members.m_lname as LastName,members.m_account as full_name,posts.p_type as ptype, category.p_type as ct_type from posts
         INNER JOIN category ON posts.post_cat = category.ca_id INNER JOIN currency ON posts.post_cur = currency.cu_id INNER JOIN
          members ON posts.post_createdBy =members.m_id INNER JOIN area ON members.m_area= area.area_id where posts.p_type=".$type." and members.m_id= ".$member_id." ORDER BY post_cdate DESC LIMIT ".$limit;
      //  print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select post_id,post_title,post_thumb,post_desc,post_cat,area.area_name as area,post_content,post_img,category.ca_type,post_price,currency.cu_currency as currency,post_cdate,post_status,post_view_count,members.m_fname as firstName,members.m_lname as LastName,members.m_account as full_name,p_type from posts INNER JOIN category ON posts.post_cat = category.ca_id INNER JOIN currency ON posts.post_cur = currency.cu_id INNER JOIN members ON posts.post_createdBy=members.m_id INNER JOIN area ON members.m_area= area.area_id ORDER BY post_cdate DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
        print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query($sql)->fetchall(PDO::FETCH_ASSOC)):null); 
      }

}

?>