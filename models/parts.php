<?php

class Parts extends Database{
    
    public function __construct(){}


    public static function limit($limit){
      $sql = "select pr_id, pr_name,parts_type.prt_title as part,brand.b_title as brand ,is_orginal,is_sold, 
      vin_no, is_new, pr_mfr, pr_detailes, pr_comment, pr_cdate, currency.cu_currency as currency, pr_price, pr_img,
       is_available, is_haraj, members.m_fname fname, members.m_lname as lname, members.m_img as avatar, pr_tag, 
       models.mo_title as model, pr_year,pr_type,store_id,store_title,store_address,store_latlang,store_detial,store_area,store_owner,store_status,users.u_account,area.area_name from parts INNER JOIN members ON parts.pr_createdBy = members.m_id INNER JOIN
        currency ON parts.pr_currency = currency.cu_id JOIN models on parts.pr_model=models.mo_id JOIN brand on parts.pr_brand = brand.b_id INNER JOIN parts_type ON parts.pr_type=parts_type.prt_id INNER JOIN  store ON parts.pr_store = store.store_id INNER JOIN area on area.area_id=store.store_area INNER JOIN users on users.u_id=store.store_owner ORDER BY pr_id DESC LIMIT ".$limit;
        
        $partsSet = (!empty(self::$dbObject))?self::$dbObject->query($sql)->fetchall(PDO::FETCH_ASSOC):null;
        if(!empty($partsSet))
         //$partsSet['delivery'] ="sddsdsd"; /*self::$dbObject->query("select * from store_shipping where ss_store=4")->fetchall(PDO::FETCH_ASSOC);*/
        
         print_r(json_encode($partsSet)); 
      }
      
      public static function isHaraj($limit){
        print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select pr_type, pr_id, pr_name,parts_type.prt_title as part,brand.b_title as brand ,is_orginal,is_sold, vin_no, is_new, pr_mfr, pr_detailes, pr_comment, pr_cdate, currency.cu_currency as currency, pr_price, pr_img, is_available, is_haraj, members.m_fname fname, members.m_lname as lname,members.m_img as avatar, pr_tag, models.mo_title as model, pr_year from parts INNER JOIN members ON parts.pr_createdBy = members.m_id INNER JOIN currency ON parts.pr_currency = currency.cu_id JOIN models on parts.pr_model=models.mo_id JOIN brand on parts.pr_brand = brand.b_id INNER JOIN parts_type ON parts.pr_type=parts_type.prt_id where is_haraj=1 ORDER BY pr_id DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
      }

      public static function isStore($limit){
        //var_dump(self::$dbObject);
        print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select pr_type, pr_id, pr_name,parts_type.prt_title as part,brand.b_title as brand ,is_orginal,is_sold, vin_no, is_new, pr_mfr, pr_detailes, pr_comment, pr_cdate, currency.cu_currency as currency, pr_price, pr_img, is_available, is_haraj, members.m_fname fname, members.m_lname as lname, members.m_img as avatar, pr_tag, models.mo_title as model, pr_year from parts INNER JOIN members ON parts.pr_createdBy = members.m_id INNER JOIN currency ON parts.pr_currency = currency.cu_id JOIN models on parts.pr_model=models.mo_id JOIN brand on parts.pr_brand = brand.b_id INNER JOIN parts_type ON parts.pr_type=parts_type.prt_id where is_haraj=0 ORDER BY pr_id DESC LIMIT ".$limit)->fetchall(PDO::FETCH_ASSOC)):null); 
      }

      public static function partType(){
        //var_dump(self::$dbObject);
        print_r((!empty(self::$dbObject))?json_encode(self::$dbObject->query("select * from parts_type")->fetchall(PDO::FETCH_ASSOC)):null); 
      }
      

}

?>