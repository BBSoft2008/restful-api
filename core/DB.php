<?php

interface IconnectionManager
{
   public function init();
}


class MySQL implements IconnectionManager{
     
    public function __construct(){}
  
    public function init(){
        global $config;
        return new PDO('mysql:host='.$config['DB']['mysql']['host'].';dbname='.$config['DB']['mysql']['dbname'].';charset='.$config['DB']['mysql']['charset'], $config['DB']['mysql']['user'], $config['DB']['mysql']['password'], array(PDO::ATTR_EMULATE_PREPARES => false,PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
      }

}

class SQLite{}

class Postgre{}

class Database
{

 protected static $dbObject = null;
 protected static $db = null;
 
 private function __construct(){}
 private function __clone(){}


  public static function engage(IconnectionManager $db){

    if (self::$dbObject == null)
    {
      self::$dbObject = new Database();
      self::$dbObject = $db->init();
      //return self::$dbObject;       
      
    }else{
      self::$dbObject = null;
      echo 'Can`t create another instance...<br/>';
    }

    //return new static;
  }


  public function columns(){
    return array_keys(self::$dbObject->query('select * from '.strtolower(get_called_class()))->fetch(PDO::FETCH_ASSOC));
  }

  public static function all(){
    return (!empty(self::$dbObject))?self::$dbObject->query('select * from '.strtolower(get_called_class()))->fetchall(PDO::FETCH_ASSOC):null; 
  }

  public function getId($id){
   return self::$dbObject->query('select * from '.strtolower(get_called_class()).' where '.$this->columns()[0].'='.$id)->fetch(PDO::FETCH_ASSOC); 
  }


  public static function insert($query = []){
    //get used class name as table name in query.
    //$table = end(explode('\\',get_called_class()));
    $sql='insert into '.strtolower(get_called_class()).'()values(';
  
    foreach($query as $val){
         $sql.='?,';
    }
  
    $sql=rtrim($sql,',');
    $sql.=')';
  
   // echo $sql;
    $res = self::$dbObject->prepare($sql);
    if($res->execute($query))
    print_r(json_encode(['message'=>'Created successfully.','status'=>true]));
    else
       print_r(json_encode(['message'=>$res->errorCode(),'status'=>false]));
  
  }


  
  

  public static function update($query = [],$con=[]){
    //get used class name as table name in query.
    //$table = end(explode('\\',get_called_class()));
    $sql='update '.strtolower(get_called_class()).'set ';
  
    foreach($query as $key=>$m){
         $sql.=$key.'='.'?,';
    }
  
    $sql=rtrim($sql,',');
    $sql.=' where ';
    foreach($con as $k=>$v){
    $sql.= $k.'="'.$v.'"';
    }

    echo $sql;
    /*$res = self::$dbObject->prepare($sql);
    if($res->execute($query))
    print_r(json_encode(['message'=>'Created successfully.','status'=>true]));
    else
       print_r(json_encode(['message'=>$res->errorCode(),'status'=>false]));*/
  
  }


  
}


?>
