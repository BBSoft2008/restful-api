<?php

global $config;
$config = require_once 'core/env.php';
require_once 'core/DB.php';

//JWT...
require_once 'core/src/BeforeValidException.php';
require_once 'core/src/ExpiredException.php';
require_once 'core/src/SignatureInvalidException.php';
require_once 'core/src/JWK.php';
require_once 'core/src/JWT.php';

$fileList = glob('models/*');
 
foreach($fileList as $filename){
   require_once $filename; 
}

?>